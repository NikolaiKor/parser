package core.parsers;

import data.ErrorLinks;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import properties.PropertiesStorage;

import javax.xml.ws.http.HTTPException;
import java.io.IOException;

/**
 * Class for taking the HTML page by url and creating Jsoup document
 *
 * @author Koropatnik
 */
public class Connector {
    /**
     * Information about browser, that is received during connection.
     * Use for masking and get page in needed state (for example, not for mobile).
     */
    private static final String USER_AGENT = PropertiesStorage.get("connection.userAgent");

    /**
     * Timeout of connection to page
     */
    private static final int TIMEOUT = Integer.parseInt(PropertiesStorage.get("connection.timeout"));

    /**
     * Maximal number of unsuccessful attempts. If all connections unsuccessful, link is bad
     */
    private static final int CONNECTION_ATTEMPTS = Integer.parseInt(PropertiesStorage.get("connection.connectionAttempts"));
    String type;


    public Connector(String type) {
        this.type = type;
    }

    /**
     * Return the document Jsoup with page by the url or null, if url is bad
     *
     * @param url the url of requested page
     * @return the document Jsoup with page by the url or null, if url is bad
     */
    public Document getDocumentByURL(String url) {
        return documentConnect(url, 0);
    }

    /**
     * Return the document Jsoup with page by the url or null, if url is bad
     *
     * @param url   url the url of requested page
     * @param depth the number of current attempt connecting
     * @return the document Jsoup with page by the url or null, if url is bad
     */
    private Document documentConnect(String url, int depth) {
        Document document = null;
        try {
            document = Jsoup.connect(url).userAgent(USER_AGENT).timeout(TIMEOUT).get();
            System.out.println("Reading done");
        } catch (IOException e) {
            if (depth < CONNECTION_ATTEMPTS) {
                getDocumentByURL(url);
            } else {
                setErrorUrl(url, type);
            }
        } catch (HTTPException e) {
            if (depth < 5 || e.getStatusCode() != 404) {
                getDocumentByURL(url);
            } else {
                setErrorUrl(url, type);
            }
        }
        return document;
    }

    /**
     * Set bad url into ErrorLinks storage
     *
     * @param url  bad url
     * @param type type of parser that use this Connector - menu, pages or product
     */
    private void setErrorUrl(String url, String type) {
        switch (type) {
            case "target":
                ErrorLinks.getInstance().addErrorURL(url);
                break;
            case "menu":
                ErrorLinks.getInstance().addErrorMenuURL(url);
                break;
            case "pages":
                ErrorLinks.getInstance().addErrorPageUrl(url);
                break;
        }
    }
}