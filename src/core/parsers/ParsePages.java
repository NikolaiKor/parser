package core.parsers;

import data.DataStorage;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Class for parsing the product's list in each category.
 * It use the structure which forms the ParseMenu class.
 * List of products is separated on pages.
 * In this case search link to page with the list of all products and connect to it.
 * Parse links to product's page and create empty object of product in DataStorage.
 *
 * @author Koropatnik
 */
public class ParsePages {
    /**
     * is used for connection to page by url
     */
    Connector connector;

    public ParsePages() {
        connector = new Connector("pages");
    }

    /**
     * Parse the list of products by url from storage object.
     * Not parse children storage.
     *
     * @param storage storage, which page is parse
     */
    public void parse(DataStorage storage) {
        Document doc = connector.getDocumentByURL(storage.getUrl());
        if (doc == null) {
            return;
        }
        String all = getAll(doc);
        if (all != null) {
            parsePage(all, storage);
        }
    }

    /**
     * Parse the list of products by url of the list of all products of storage's own category(except children).
     *
     * @param url     url of page with list of all products of storage's category
     * @param storage storage, which page is parse
     */
    private void parsePage(String url, DataStorage storage) {
        Document doc = connector.getDocumentByURL(url);
        if (doc == null) {
            return;
        }

        Elements elements = doc.select("li div.name a");
        for (Element element : elements) {
            String urlParsed = element.attr("href");
            if (urlParsed.indexOf("http://proteinchik.com.ua/") != 0) {
                urlParsed = "http://proteinchik.com.ua/" + urlParsed;
            }
            storage.addInfo(urlParsed);
        }
    }

    /**
     * convert product's list, separated on pages, to one page
     * (link to page "View all")
     *
     * @param doc document with 1 page of product's list
     * @return url of page with all products of this category
     */
    private String getAll(Document doc) {
        Elements urls = doc.select("a.pageResults");
        if (!urls.isEmpty()) {
            return urls.last().attr("href");
        }
        return null;
    }

    /*
    private void parseByPages() {
        parsePage(storage.getUrl());
        int page = 1;
        String nextPageUrl = getNextPageUrl();
        page++;
        while (nextPageUrl != null) {
            parsePage(nextPageUrl);
            page++;
        }
    }

    private String getNextPageUrl(Document doc) {
        Elements urls = doc.select("a.pageResults[title=Следующая]");
        if (!urls.isEmpty()) {
            return urls.first().attr("href");
        }
        return null;
    }*/
}
