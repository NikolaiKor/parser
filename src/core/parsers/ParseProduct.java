package core.parsers;

import core.Formatter;
import data.ProductInfo;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Class for parsing information about product from page with it's description
 *
 * @author Koropatnik
 */
public class ParseProduct {
    /**
     * is used for connection to page by url
     */
    Connector connector;

    public ParseProduct() {
        connector = new Connector("target");
    }

    /**
     * Parse information about product(manufacturer, price, availability etc.) from it's page
     *
     * @param product product, whose page parse
     */
    public void parse(ProductInfo product) {
        Document doc = connector.getDocumentByURL(product.getUrl());
        if (doc == null) {
            return;
        }
        String priceStr[];
        Elements elements = doc.select("td[class=products_info_td]");
        Element root = elements.first();

        product.setName(root.getElementsByTag("h3").text());
        product.setManufacturer(root.getElementsByTag("div").text().split(":")[1].trim());

        Elements stockInformation = elements.select("td.p_price");
        priceStr = stockInformation.select("span:not(.quan)").first().text().trim().split(" ");
        product.setPrice(priceStr[0]);
        product.setCurrency(priceStr[1].substring(0, priceStr[1].length() - 1));
        product.setAvailability(stockInformation.select(".quan").first().text());
        product.setDescription(getDescription(doc));

        product.setImg(doc.select("a.zoom").first().attr("href"));
        //System.out.println(product);
    }

    /**
     * get product's description
     *
     * @param document document from page with product information
     * @return product's description
     */
    private String getDescription(Document document) {//Need to solve problem with text's format saving!!!!
        Elements descriptionElements = document.select("div#desc");
        String desc = "";
        Formatter formatter = new Formatter();
        if (!descriptionElements.isEmpty()) {
            for (Element element : descriptionElements) {
                desc += formatter.getPlainText(element);
            }
        }
        return formatter.removeEnters(desc);
    }
}
