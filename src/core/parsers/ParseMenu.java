package core.parsers;

import data.DataStorage;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Class for parsing site's menu(or structure). During parsing the structure of DataStorage forms.
 *
 * @author Koropatnik
 */
public class ParseMenu {
    /**
     * is used for connection to page by url
     */
    Connector connector;
    /**
     * root of data storage
     */
    DataStorage storage;

    /**
     * @param storage root of dataStorage
     */
    public ParseMenu(DataStorage storage) {
        connector = new Connector("menu");
        this.storage = storage;
    }

    /**
     * Parse structure of site's main menu.
     * In this case menu has 2 levels and levels are different by "padding-left" attribute of "li" tag
     *
     * @param url url of page
     */
    public void parse(String url) {
        Document doc = connector.getDocumentByURL(url);
        if (doc == null) {
            return;
        }

        DataStorage lastCreated = null;
        Elements elements = doc.select("div#categoriesBoxMenu li");
        for (Element menu : elements) {
            Elements href = menu.children().select("a");
            if (menu.attr("style").compareToIgnoreCase("padding-left:15px") != 0) {
                for (Element element : href) {
                    lastCreated = storage.addChild(element.ownText(), element.attr("href"));
                }
            } else {
                for (Element element : href) {
                    lastCreated = lastCreated.addChild(element.ownText(), element.attr("href"));
                }
            }
        }
    }
}
