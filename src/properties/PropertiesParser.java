package properties;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * Class for parsing configuration from XML
 * Must be rewrite
 *
 * @author Koropatnik
 */
public class PropertiesParser {

    /**
     * parse XML file with configuration
     *
     * @param location location of XML file
     */
    public void parseFile(String location) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document doc = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        try {
            doc = builder.parse(new File(location));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String key = "";
        Element root = doc.getDocumentElement();
        key = root.getNodeName();
        NodeList children = root.getChildNodes();
        parseStructure(children, "");
    }

    /**
     * recursively parse XML and add properties
     *
     * @param nodeList list of nodes for parsing
     * @param s        part of name the next property(parent nodes, separated by dot)
     */
    private void parseStructure(NodeList nodeList, String s) {
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node child = nodeList.item(i);
            if (child instanceof Element) {
                Element childElement = (Element) child;
                if (child.hasChildNodes()) {
                    parseStructure(child.getChildNodes(), s + "." + child.getNodeName());
                }
            }

            if (child instanceof Text) {
                Text textNode = (Text) child;
                String text = textNode.getData().trim();
                if (text.compareTo("") != 0) {
                    PropertiesStorage.getInstance().put(s.substring(1), text);
                }
            }

        }
    }
}
