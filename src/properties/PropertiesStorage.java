package properties;

import java.util.Properties;

/**
 *
 */
public class PropertiesStorage {
    private static PropertiesStorage propertiesStorage;
    private static Properties properties;

    private PropertiesStorage() {
        properties = new Properties();
    }

    public static PropertiesStorage getInstance() {
        if (propertiesStorage == null) {
            propertiesStorage = new PropertiesStorage();
        }
        return propertiesStorage;
    }

    public static String get(String key){
        return (String)properties.get(key);
    }

    public static void put(String key, String value){
        properties.put(key,value);
    }

    /*private void init(){
    }*/

}
