package data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Nikolai on 03.07.2015.
 */
@XmlRootElement(name = "product")
@XmlType(propOrder = {"name", "manufacturer", "price", "currency", "availability", "description", "img", "url"})
public class ProductInfo {
    private String url;
    private String availability;
    private String manufacturer;
    private String currency;
    private String price;
    private String name;
    private String description;
    private String img;

    public String getImg() {
        return img;
    }

    @XmlElement(name = "img")
    public void setImg(String img) {
        this.img = img;
    }

    public ProductInfo() {
    }

    public ProductInfo(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    @XmlElement(name = "url")
    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    @XmlElement(name = "description")
    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public String getAvailability() {
        return availability;
    }

    @XmlElement(name = "availability")
    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    @XmlElement(name = "manufacturer")
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getCurrency() {
        return currency;
    }

    @XmlElement(name = "currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPrice() {
        return price;
    }

    @XmlElement(name = "price")
    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Name: " + name +
                "\nManufacturer: " + manufacturer +
                "\nPrice: " + price +
                "\nCurrency: " + currency +
                "\nAvailability: " + availability +
                "\nDescription:\n" + description +
                "\nImg: " + img;
    }
}
