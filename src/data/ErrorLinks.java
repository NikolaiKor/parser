package data;

import java.util.ArrayList;

/**
 * Created by Nikolai on 04.07.2015.
 */
public class ErrorLinks {
    private static ErrorLinks links = null;
    private ArrayList<String> errorParsingURL;
    private ArrayList<String> errorGroupURL;


    private ArrayList<String> errorPageUrl;

    public ErrorLinks() {
        this.errorParsingURL = new ArrayList<String>();
        this.errorGroupURL = new ArrayList<String>();
        errorPageUrl = new ArrayList<String>();
    }

    public static ErrorLinks getInstance() {
        if (links == null) {
            links = new ErrorLinks();
        }
        return links;
    }

    public void addErrorURL(String url) {
        errorParsingURL.add(url);
    }

    public ArrayList<String> getErrorGroupURL() {
        return errorGroupURL;
    }

    public void addErrorMenuURL(String url) {
        errorGroupURL.add(url);
    }

    public ArrayList<String> getErrorURL() {
        return errorParsingURL;
    }

    public ArrayList<String> getErrorPageUrl() {
        return errorPageUrl;
    }

    public void addErrorPageUrl(String url) {
        this.errorPageUrl.add(url);
    }
}
