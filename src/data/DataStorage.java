package data;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;

/**
 * Created by Nikolai on 03.07.2015.
 */
@XmlSeeAlso({ProductInfo.class})
@XmlRootElement(name = "storage")
@XmlType(propOrder = {"name", "products", "url", "childStorage"})
public class DataStorage {
    public DataStorage(String name, String url, DataStorage parent) {
        products = new ArrayList<>();
        this.name = name;
        childStorage = new ArrayList<>();
        this.url = url;
        this.parent = parent;
    }
    public DataStorage(){}

    public DataStorage(String name, String url) {
        products = new ArrayList<>();
        this.name = name;
        childStorage = new ArrayList<>();
        this.url = url;
        this.parent = null;
    }

    private DataStorage parent;
    private ArrayList<ProductInfo> products;
    private String name;
    private ArrayList<DataStorage> childStorage;
    private String url;

    public ArrayList<DataStorage> getChild() {
        return childStorage;
    }

    public synchronized DataStorage getStorageByName(String name) {
        for (DataStorage storage : childStorage) {
            if (storage.name.compareTo(name) == 0) {
                return storage;
            }
        }
        for (DataStorage storage : childStorage) {
            storage.getStorageByName(name);
        }
        return null;
    }

    public synchronized ArrayList<ProductInfo> getProducts() {
        return products;
    }

    public synchronized void addInfo(ProductInfo info) {
        products.add(info);
    }

    public synchronized ProductInfo addInfo(String url) {
        ProductInfo info = new ProductInfo(url);
        products.add(info);
        return info;
    }

    public String getName() {
        return name;
    }

    /*@Override
    public synchronized String toString() {
        String s = "";
        for (TargetInfo info : products) {
            s += info.toString();
        }
        return s;
    }*/

    public DataStorage addChild(String name, String url) {
        DataStorage child = new DataStorage(name, url, this);
        childStorage.add(child);
        return child;
    }

    public String getUrl() {
        return url;
    }

    @XmlElement(name = "url")
    public void setUrl(String url) {
        this.url = url;
    }

    public String toString(int depth) {
        String buf = "";
        for (int i = 1; i < depth; i++) {
            buf += "\t";
        }
        String s = buf + name + "\t(" + products.size() + ")\n";
        for (DataStorage storage : childStorage) {
            s += storage.toString(depth + 1);
        }
        return s;
    }

    public ArrayList<DataStorage> list() {
        ArrayList<DataStorage> list = new ArrayList<DataStorage>();
        if (name != "root") {
            list.add(this);
        }
        for (DataStorage child : childStorage) {
            list.addAll(child.list());
        }
        return list;
    }

    @XmlElement(name = "products")
    @XmlElementWrapper
    public void setProducts(ArrayList<ProductInfo> products) {
        this.products = products;
    }

    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "childStorage")
    @XmlElementWrapper
    public void setChildStorage(ArrayList<DataStorage> childStorage) {
        this.childStorage = childStorage;
    }

    public ArrayList<DataStorage> getChildStorage() {
        return childStorage;
    }
}
