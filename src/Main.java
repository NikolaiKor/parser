import core.parsers.ParseMenu;
import core.parsers.ParsePages;
import core.parsers.ParseProduct;
import data.DataStorage;
import export.excel.ExcelExport;
import export.xml.XmlExport;
import properties.PropertiesParser;
import properties.PropertiesStorage;

import javax.xml.bind.JAXBException;

/**
 * Created by Nikolai on 02.07.2015.
 */
public class Main {
    private static final String PROPERTIES_LOCATION = "files\\Properties.xml";
    private static final String EXPORT_LOCATION = "files\\person.xml";

    public static void main(String[] src) {
        PropertiesStorage.getInstance();
        PropertiesParser propertiesParser = new PropertiesParser();
        propertiesParser.parseFile(PROPERTIES_LOCATION);

        DataStorage dataStorage = new DataStorage("root", "");
        ParseMenu parseMenu = new ParseMenu(dataStorage);
        ParsePages parsePages = new ParsePages();
        ParseProduct parseProduct = new ParseProduct();
        /*parseMenu.parse("http://proteinchik.com.ua");

        ArrayList<DataStorage> data = dataStorage.list();
        int i = 0;
        for (DataStorage type : data) {
            parsePages.parse(type);
            System.out.println(++i);
        }
        System.out.println(dataStorage.toString(0));
        System.out.println(data.get(6).getName());
        for (ProductInfo product : data.get(6).getProducts()) {
            parseProduct.parse(product);
        }
        for (ProductInfo product : data.get(6).getProducts()) {
            System.out.println(product);
        }*/
        XmlExport export = new XmlExport();
        DataStorage testStorage = null;
        try {
            //export.saveObject(dataStorage);
            testStorage = export.getStorage(EXPORT_LOCATION);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ExcelExport excelExport = new ExcelExport();
        try {
            excelExport.saveStorage("files\\export.xls", testStorage);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }
}
